'''
Run this Python3 script on your local machine like this:
python3 d1.py test -v4 -nodb -port 50000 -dlist exercise/d1/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:50000/exercise/d1/1#dbase=no')
'''
from tango import (
    DevDouble,
    DevState,
    DevVoid,
    DeviceProxy,
    EventData,
    EventType,
    DevFailed,
)

from tango.exception import (
    Except
)

from tango.server import (
    Device,
    attribute,
    command,
    run
)

class D1(Device):
    def init_device(self) -> None:
        super().init_device()
        self.info_stream('Hello world! This is D1.')
        self._d1_attr1 = 1.2345
        self._d1_attr2 = 5.4321

        self.set_change_event('d1_attr1', True, True)
        self.set_change_event('d1_attr2', True, True)

        self.change_subscription_id = None
        self.archive_subscription_id = None

        self.dp = DeviceProxy('tango://127.0.0.1:50001/exercise/d2/1#dbase=no')

        self.set_state(DevState.ON)

    def delete_device(self) -> None:
        try:
            if self.change_subscription_id is not None:
                self.dp.unsubscribe_event(self.change_subscription_id)
            if self.archive_subscription_id is not None:
                self.dp.unsubscribe_event(self.archive_subscription_id)
        except DevFailed as ex:
            Except.print_exception(ex)
        except Exception as ex:
            self.info_stream(f'{ex}')
        self.set_state(DevState.OFF)
        self.info_stream('D1 says good bye.')

    def cb_change_event(self, event: EventData) -> None:
        if event.err is False:
            self._d1_attr1 = event.attr_value.value
            self.push_change_event('d1_attr1', self._d1_attr1)
        else:
            self.info_stream(f'Received an error event when a CHANGE_EVENT was expected. The full content of the error event is:\n{event}\nWill continue with normal operation.')

    def cb_archive_event(self, event: EventData) -> None:
        if event.err is False:
            self._d1_attr2 = event.attr_value.value
            self.push_change_event('d1_attr2', self._d1_attr2)
        else:
            self.info_stream(f'Received an error event when an ARCHIVE_EVENT was expected. The full content of the error event is:\n{event}\nWill continue with normal operation.')

    @attribute(abs_change = 0.01)
    def d1_attr1(self) -> float:
        return self._d1_attr1

    @attribute(abs_change = 0.01)
    def d1_attr2(self) -> float:
        return self._d1_attr2

    @command
    def modify_d2_ro_attribute_value(self, new_value: float) -> None:
        if self.change_subscription_id is None:
            self.change_subscription_id = self.dp.subscribe_event('d2_my_ro_attribute', EventType.CHANGE_EVENT, self.cb_change_event)
        self.dp.d2_modify_ro_attribute_value(new_value)

    @command
    def modify_d2_rw_attribute_value(self, new_value: float) -> None:
        if self.archive_subscription_id is None:
            self.archive_subscription_id = self.dp.subscribe_event('d2_my_rw_attribute', EventType.ARCHIVE_EVENT, self.cb_archive_event)
        self.dp.d2_modify_rw_attribute_value(new_value)


def main(args = None, **kwargs):
    return run((D1,), **kwargs)

if __name__ == '__main__':
    main()
