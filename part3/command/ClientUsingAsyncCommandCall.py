#! /usr/bin/env python3
'''
In order to be able to use this script, you first need to run the
example device that blocks when the modify_ro_attribute_value command is called.
Run the DeviceServer on your local machine in a terminal like this:
python3 DeviceWithCommandBlocksOtherClients.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then, in a new terminal, run this script:
python3 ClientUsingAsyncCommandCall.py
'''


from tango import (
    ApiUtil,
    DeviceProxy,
    cb_sub_model
)
from concurrent.futures import Future
from time import sleep
from datetime import datetime

dp = DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
api_util = ApiUtil.instance()

def p(s):
    print(f'{datetime.now()} {s}')

def foo1():
    api_util.set_asynch_cb_sub_model(cb_sub_model.PUSH_CALLBACK)
    future = Future()
    p('Calling modify_ro_attribute_value...')
    dp.command_inout_asynch('modify_ro_attribute_value', 54321, future.set_result)
    p('modify_ro_attribute_value called. Waiting for the result...')
    result = future.result(timeout = 30.0)
    p('Result received.')
    value = result.argout
    p(f'{value}\n\n')

def foo2():
    api_util.set_asynch_cb_sub_model(cb_sub_model.PUSH_CALLBACK)
    def cmd_ended(result):
        import datetime
        p(f'Yo! {result}, {result.argout}\n\n')
    p('Calling modify_ro_attribute_value...')
    dp.command_inout_asynch('modify_ro_attribute_value', 54321, cmd_ended)
    p(f'modify_ro_attribute_value called. Waiting for the result...')
    sleep(6)

def foo3():
    api_util.set_asynch_cb_sub_model(cb_sub_model.PUSH_CALLBACK)
    p('Calling modify_ro_attribute_value...')
    eid = dp.command_inout_asynch('modify_ro_attribute_value', 9876)
    p('modify_ro_attribute_value called. Waiting for the result...')
    value = dp.command_inout_reply(eid, timeout=30000)
    p('Result received.')
    p(f'{value}\n\n')

foo1()
foo2()
foo3()
