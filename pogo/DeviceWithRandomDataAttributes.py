# -*- coding: utf-8 -*-
#
# This file is part of the DeviceWithRandomDataAttributes project
#
# Copyright 2024 SKA Observatory
#
# Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions
# are met:
# 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
# 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
# 3. Neither the name of the copyright holder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS ``AS IS`` AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#
# Distributed under the terms of the BSD3 license.
# See LICENSE.txt for more info.

"""
Device with random data attributes

A device with 22 attributes, 20 scalars, 1 a 1d-arrays and 1 a 2d-array.
The scalars will contain a random number that is generated on read.
The arrays will contains zeros but a random number in element [76]
of the 1d-arrasy and element [76][76] of the 2d-array.
"""

# PROTECTED REGION ID(DeviceWithRandomDataAttributes.system_imports) ENABLED START #
# PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.system_imports

# PyTango imports
import tango
from tango import DebugIt
from tango.server import run
from tango.server import Device
from tango.server import attribute, command
from tango import AttrQuality, DispLevel, DevState
from tango import AttrWriteType, PipeWriteType
# Additional import
# PROTECTED REGION ID(DeviceWithRandomDataAttributes.additionnal_import) ENABLED START #
from tango import (
    DevFailed,
    DevState,
    EnsureOmniThread
)

from numpy import (
    zeros
)
from numpy.random import (
    random
)

from time import (
    sleep
)

from threading import (
    Thread
)
# PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.additionnal_import

__all__ = ["DeviceWithRandomDataAttributes", "main"]


class DeviceWithRandomDataAttributes(Device):
    """
    A device with 22 attributes, 20 scalars, 1 a 1d-arrays and 1 a 2d-array.
    The scalars will contain a random number that is generated on read.
    The arrays will contains zeros but a random number in element [76]
    of the 1d-arrasy and element [76][76] of the 2d-array.
    """
    # PROTECTED REGION ID(DeviceWithRandomDataAttributes.class_variable) ENABLED START #
    # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.class_variable

    # ----------
    # Attributes
    # ----------

    rnd1 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd2 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd3 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd4 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd5 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd6 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd7 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd8 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd9 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd10 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd11 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd12 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd13 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd14 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd15 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd16 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd17 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd18 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd19 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd20 = attribute(
        dtype='DevDouble',
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    delay = attribute(
        dtype='DevDouble',
        access=AttrWriteType.READ_WRITE,
    )

    event_cycle_time = attribute(
        dtype='DevDouble',
        access=AttrWriteType.READ_WRITE,
    )

    rnd100 = attribute(
        dtype=('DevDouble',),
        max_dim_x=1000,
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    rnd1000 = attribute(
        dtype=(('DevDouble',),),
        max_dim_x=1000, max_dim_y=1000,
        abs_change=0.1,
        archive_abs_change=0.1,
        max_value=1.0,
        min_value=0.0,
        max_alarm=1.0,
        min_alarm=0.99,
        max_warning=0.99,
        min_warning=0.98,
    )

    # ---------------
    # General methods
    # ---------------

    def init_device(self):
        """Initializes the attributes and properties of the DeviceWithRandomDataAttributes."""
        Device.init_device(self)
        self.rnd1.set_data_ready_event(True)
        self.set_change_event("rnd1", True, True)
        self.set_archive_event("rnd1", True, True)
        self.rnd2.set_data_ready_event(True)
        self.set_change_event("rnd2", True, True)
        self.set_archive_event("rnd2", True, True)
        self.rnd3.set_data_ready_event(True)
        self.set_change_event("rnd3", True, True)
        self.set_archive_event("rnd3", True, True)
        self.rnd4.set_data_ready_event(True)
        self.set_change_event("rnd4", True, True)
        self.set_archive_event("rnd4", True, True)
        self.rnd5.set_data_ready_event(True)
        self.set_change_event("rnd5", True, True)
        self.set_archive_event("rnd5", True, True)
        self.rnd6.set_data_ready_event(True)
        self.set_change_event("rnd6", True, True)
        self.set_archive_event("rnd6", True, True)
        self.rnd7.set_data_ready_event(True)
        self.set_change_event("rnd7", True, True)
        self.set_archive_event("rnd7", True, True)
        self.rnd8.set_data_ready_event(True)
        self.set_change_event("rnd8", True, True)
        self.set_archive_event("rnd8", True, True)
        self.rnd9.set_data_ready_event(True)
        self.set_change_event("rnd9", True, True)
        self.set_archive_event("rnd9", True, True)
        self.rnd10.set_data_ready_event(True)
        self.set_change_event("rnd10", True, True)
        self.set_archive_event("rnd10", True, True)
        self.rnd11.set_data_ready_event(True)
        self.set_change_event("rnd11", True, True)
        self.set_archive_event("rnd11", True, True)
        self.rnd12.set_data_ready_event(True)
        self.set_change_event("rnd12", True, True)
        self.set_archive_event("rnd12", True, True)
        self.rnd13.set_data_ready_event(True)
        self.set_change_event("rnd13", True, True)
        self.set_archive_event("rnd13", True, True)
        self.rnd14.set_data_ready_event(True)
        self.set_change_event("rnd14", True, True)
        self.set_archive_event("rnd14", True, True)
        self.rnd15.set_data_ready_event(True)
        self.set_change_event("rnd15", True, True)
        self.set_archive_event("rnd15", True, True)
        self.rnd16.set_data_ready_event(True)
        self.set_change_event("rnd16", True, True)
        self.set_archive_event("rnd16", True, True)
        self.rnd17.set_data_ready_event(True)
        self.set_change_event("rnd17", True, True)
        self.set_archive_event("rnd17", True, True)
        self.rnd18.set_data_ready_event(True)
        self.set_change_event("rnd18", True, True)
        self.set_archive_event("rnd18", True, True)
        self.rnd19.set_data_ready_event(True)
        self.set_change_event("rnd19", True, True)
        self.set_archive_event("rnd19", True, True)
        self.rnd20.set_data_ready_event(True)
        self.set_change_event("rnd20", True, True)
        self.set_archive_event("rnd20", True, True)
        self.rnd100.set_data_ready_event(True)
        self.set_change_event("rnd100", True, True)
        self.set_archive_event("rnd100", True, True)
        self.rnd1000.set_data_ready_event(True)
        self.set_change_event("rnd1000", True, True)
        self.set_archive_event("rnd1000", True, True)
        self._rnd1 = 0.0
        self._rnd2 = 0.0
        self._rnd3 = 0.0
        self._rnd4 = 0.0
        self._rnd5 = 0.0
        self._rnd6 = 0.0
        self._rnd7 = 0.0
        self._rnd8 = 0.0
        self._rnd9 = 0.0
        self._rnd10 = 0.0
        self._rnd11 = 0.0
        self._rnd12 = 0.0
        self._rnd13 = 0.0
        self._rnd14 = 0.0
        self._rnd15 = 0.0
        self._rnd16 = 0.0
        self._rnd17 = 0.0
        self._rnd18 = 0.0
        self._rnd19 = 0.0
        self._rnd20 = 0.0
        self._delay = 0.0
        self._event_cycle_time = 0.0
        self._rnd100 = (0.0,)
        self._rnd1000 = ((0.0,),)
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.init_device) ENABLED START #
        self.debug_stream(f'User part in init_device of device {self.get_name()} done.')
        self._delay = 1.0
        self._event_cycle_time = 5.0
        self._stop = False
        self._thread = Thread(target = self._push_event_thread)
        self._thread.start()
        self.set_state(DevState.ON)
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.init_device

    def always_executed_hook(self):
        """Method always executed before any TANGO command is executed."""
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.always_executed_hook) ENABLED START #
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.always_executed_hook

    def delete_device(self):
        """Hook to delete resources allocated in init_device.

        This method allows for any memory or other resources allocated in the
        init_device method to be released.  This method is called by the device
        destructor and by the device Init command.
        """
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.delete_device) ENABLED START #
        self.debug_stream(f'User part in delete_device of device {self.get_name()} done.')
        self.set_state(DevState.OFF)
        self._stop = True
        # This sleep is necessary to give the omniThread a moment to stop.
        # Otherwise pressing CTRL-C to stop this DeviceServer results in a
        # segfault.
        sleep(self._delay + 1.0)
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.delete_device

    # ------------------
    # Attributes methods
    # ------------------

    def read_rnd1(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd1_read) ENABLED START #
        """Return the rnd1 attribute."""
        return self._attrs["rnd1"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd1_read
    def read_rnd2(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd2_read) ENABLED START #
        """Return the rnd2 attribute."""
        return self._attrs["rnd2"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd2_read
    def read_rnd3(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd3_read) ENABLED START #
        """Return the rnd3 attribute."""
        return self._attrs["rnd3"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd3_read
    def read_rnd4(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd4_read) ENABLED START #
        """Return the rnd4 attribute."""
        return self._attrs["rnd4"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd4_read
    def read_rnd5(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd5_read) ENABLED START #
        """Return the rnd5 attribute."""
        return self._attrs["rnd5"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd5_read
    def read_rnd6(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd6_read) ENABLED START #
        """Return the rnd6 attribute."""
        return self._attrs["rnd6"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd6_read
    def read_rnd7(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd7_read) ENABLED START #
        """Return the rnd7 attribute."""
        return self._attrs["rnd7"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd7_read
    def read_rnd8(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd8_read) ENABLED START #
        """Return the rnd8 attribute."""
        return self._attrs["rnd8"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd8_read
    def read_rnd9(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd9_read) ENABLED START #
        """Return the rnd9 attribute."""
        return self._attrs["rnd9"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd9_read
    def read_rnd10(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd10_read) ENABLED START #
        """Return the rnd10 attribute."""
        return self._attrs["rnd10"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd10_read
    def read_rnd11(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd11_read) ENABLED START #
        """Return the rnd11 attribute."""
        return self._attrs["rnd11"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd11_read
    def read_rnd12(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd12_read) ENABLED START #
        """Return the rnd12 attribute."""
        return self._attrs["rnd12"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd12_read
    def read_rnd13(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd13_read) ENABLED START #
        """Return the rnd13 attribute."""
        return self._attrs["rnd13"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd13_read
    def read_rnd14(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd14_read) ENABLED START #
        """Return the rnd14 attribute."""
        return self._attrs["rnd14"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd14_read
    def read_rnd15(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd15_read) ENABLED START #
        """Return the rnd15 attribute."""
        return self._attrs["rnd15"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd15_read
    def read_rnd16(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd16_read) ENABLED START #
        """Return the rnd16 attribute."""
        return self._attrs["rnd16"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd16_read
    def read_rnd17(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd17_read) ENABLED START #
        """Return the rnd17 attribute."""
        return self._attrs["rnd17"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd17_read
    def read_rnd18(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd18_read) ENABLED START #
        """Return the rnd18 attribute."""
        return self._attrs["rnd18"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd18_read
    def read_rnd19(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd19_read) ENABLED START #
        """Return the rnd19 attribute."""
        return self._attrs["rnd19"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd19_read
    def read_rnd20(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd20_read) ENABLED START #
        """Return the rnd20 attribute."""
        return self._attrs["rnd20"]
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd20_read
    def read_delay(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.delay_read) ENABLED START #
        """Return the delay attribute."""
        return self._delay
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.delay_read
    def write_delay(self, value):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.delay_write) ENABLED START #
        """Set the delay attribute."""
        self._delay = value
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.delay_write
    def read_event_cycle_time(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.event_cycle_time_read) ENABLED START #
        """Return the event_cycle_time attribute."""
        return self._event_cycle_time
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.event_cycle_time_read
    def write_event_cycle_time(self, value):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.event_cycle_time_write) ENABLED START #
        """Set the event_cycle_time attribute."""
        self._event_cycle_time = value
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.event_cycle_time_write
    def read_rnd100(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd100_read) ENABLED START #
        """Return the rnd100 attribute. Only item [76] is not zero but random."""
        return self._rnd100
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd100_read
    def read_rnd1000(self):
        # PROTECTED REGION ID(DeviceWithRandomDataAttributes.rnd1000_read) ENABLED START #
        """Return the rnd1000 attribute. Only item [76][76] is not zero but random."""
        if self._delay > 0.0:
            sleep(self._delay)
        return self._rnd1000
        # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.rnd1000_read
    # --------
    # Commands
    # --------


# ----------
# Run server
# ----------

# PROTECTED REGION ID(DeviceWithRandomDataAttributes.custom_code) ENABLED START #
    def _push_event_thread(self):
        with EnsureOmniThread():
            self._attrs = {}
            while self._stop is False:
                self.debug_stream('Pushing events...')
                try:
                    multi_attr = self.get_device_attr()
                    attr_list = multi_attr.get_attribute_list()
                    for index in range(0, len(attr_list)):
                        attr = attr_list[index]
                        name = attr.get_name()
                        if name.startswith("rnd"):
                            x = attr.get_max_dim_x()
                            y = attr.get_max_dim_y()
                            if x == 1 and y == 0:
                                self._attrs[name] = random()
                            elif (x > 1 and y == 0):
                                self._attrs[name] = zeros(1000)
                                self._attrs[name][76] = random()
                            elif (x > 1 and y > 0):
                                self._attrs[name] = zeros((1000, 1000))
                                self._attrs[name][76][76] = random()
                            else:
                                self.error_stream(f"Cannot determine dimensionality of attribute \"{name}\": x = {x}, y = {y}")
                    for name, value in self._attrs.items():
                        self.push_change_event(name, value)
                        self.push_archive_event(name, value)
                except DevFailed as ex:
                    self.error_stream(f"{ex}")
                except Exception as ex:
                    self.error_stream(f"{ex}")
                sleep(self._event_cycle_time)
# PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.custom_code


def main(args=None, **kwargs):
    """Main function of the DeviceWithRandomDataAttributes module."""
    # PROTECTED REGION ID(DeviceWithRandomDataAttributes.main) ENABLED START #
    return run((DeviceWithRandomDataAttributes,), args=args, **kwargs)
    # PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.main

# PROTECTED REGION ID(DeviceWithRandomDataAttributes.custom_functions) ENABLED START #
# PROTECTED REGION END #    //  DeviceWithRandomDataAttributes.custom_functions


if __name__ == '__main__':
    main()
