'''
Run this Python3 script on your local machine like this:
python3 DeviceWithChangeEventWithoutAbsChange.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''
from tango import (
    DevDouble,
    DevString,
    DevState,
    DevVoid
)
from tango.server import (
    Device,
    attribute,
    command,
    run
)

class DeviceWithChangeEventWithoutAbsChange(Device):
    def init_device(self):
        super().init_device()
        self.__my_rw_double_attribute_value = 5.4321
        self.__my_rw_string_attribute_value = 'Yo people!'
        self.set_change_event('my_rw_double_attribute_without_abs_change', True, True)
        self.set_change_event('my_rw_string_attribute_without_abs_change', True, True)
        self.set_state(DevState.ON)

    def delete_device(self):
        self.set_state(DevState.OFF)
        super().delete_device()

    # One cannot subscribe to change events of this attribute because
    # the neither the abs_change property nor the rel_change property are set
    # in the attribute decorator.
    @attribute(dtype = DevDouble)
    def my_rw_double_attribute_without_abs_change(self) -> DevDouble:
        return self.__my_rw_double_attribute_value
    @my_rw_double_attribute_without_abs_change.write
    def my_rw_double_attribute_without_abs_change(self, value: DevDouble = None) -> DevVoid:
        self.__my_rw_double_attribute_value = value
        self.push_change_event('my_rw_double_attribute_without_abs_change', value)

    # Same here, right? Right?
    @attribute(dtype = DevString)
    def my_rw_string_attribute_without_abs_change(self) -> DevString:
        return self.__my_rw_string_attribute_value
    @my_rw_string_attribute_without_abs_change.write
    def my_rw_string_attribute_without_abs_change(self, value: DevString = None) -> DevVoid:
        self.__my_rw_string_attribute_value = value
        self.push_change_event('my_rw_string_attribute_without_abs_change', value)


def main(args = None, **kwargs):
    return run((DeviceWithChangeEventWithoutAbsChange,), **kwargs)

if __name__ == '__main__':
    main()
