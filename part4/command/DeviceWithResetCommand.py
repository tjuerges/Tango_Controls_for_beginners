'''
Run this Python3 script on your local machine like this:
python3 DeviceWithResetCommand.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''

from tango import (
    DevState,
    Util
)
from tango.server import (
    Device,
    attribute,
    command,
    run
)


class DeviceWithResetCommand(Device):
    def init_device(self):
        super().init_device()
        # Set the initial value of my read-only-attribute
        self.__my_ro_attribute_value = 1.2345
        # Set the initial value of my read-write-attribute
        self.__my_rw_attribute_value = 5.4321
        self.set_state(DevState.ON)

    @attribute()
    def my_ro_attribute(self) -> float:
        return self.__my_ro_attribute_value

    @attribute()
    def my_rw_attribute(self) -> float:
        return self.__my_rw_attribute_value
    @my_rw_attribute.write
    def my_rw_attribute(self, value: float = None) -> None:
        self.__my_rw_attribute_value = value

    @command()
    def reset(self) -> None:
        util = Util.instance()
        adm = util.get_dserver_device()
        adm.restart(self.get_name())

    @command()
    def set_my_ro_attribute(self, value: float) -> None:
        self.__my_ro_attribute_value = value

    @command()
    def read_my_rw_attribute(self) -> float:
        return self.__my_rw_attribute_value


def main(args = None, **kwargs):
    return run((DeviceWithResetCommand,), **kwargs)

if __name__ == '__main__':
    main()
