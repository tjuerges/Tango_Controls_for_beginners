'''
Run this Python3 script on your local machine like this:
python3 DeviceWithAsyncioLongRunningCommand.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''

from tango import (
    DevDouble,
    DevState,
    DevString,
    DevVoid,
    GreenMode
)

from tango.server import (
    attribute,
    command,
    Device,
    run
)
from json import (
    dumps,
    loads
)
from datetime import datetime
from numpy.random import random
import asyncio

# I want to print neat ISO timestamps.
def now():
    return datetime.utcnow().strftime('%FT%T.%f')[:-3]

class AsyncLongRunningCommand(Device):
    green_mode = GreenMode.Asyncio

    async def init_device(self):
        await super().init_device()

        # This is a current "state" of all long running commands.
        # The attribute commandState returns this value on read.
        self._commandState = dumps({})

        # And this is just a random number that gets renewed
        # in a coroutine every 0.1s.
        # This value is returned then the randomNumber attribute
        # is read.
        self._randomNumber = random()

        # This signals the coroutine which modifies the random
        # number to run for as long as the value is True.
        self._thread_run = True

        # This is the pseudo "duration" in s of the long
        # running command.
        self._waitTime = 5.0

        # This is for how long the coroutine which updates the
        # random number sleeps.
        self._updateSleep = 1.0

        # Enable the manual sending of change and archive events
        # without a DeviceServer polling loop and without checking
        # of the value.
        self.set_change_event('commandState', True, False)
        self.set_change_event('my_ro_attribute', True, False)
        self.set_archive_event('commandState', True, False)
        self.set_archive_event('my_ro_attribute', True, False)

        # Start a coroutine which updates the randomNumber
        # attribute every .
        self._update_randomNumber_task = asyncio.create_task(self.update_randomNumber())

        # Be nice.
        self.set_state(DevState.ON)


    async def update_commandState(self, newState = None) -> None:
        '''
        Update the commandState attribute. Called as needed.
        '''
        tmpCommandState = loads(self._commandState)
        command_id = newState['command_id']
        new_state = {newState['timestamp']: newState['state']}
        current_state = tmpCommandState.get(command_id)
        if current_state is None:
            states = new_state
        else:
            states = current_state | new_state
        tmpCommandState[command_id] = states
        json_state = dumps(tmpCommandState)
        self._commandState = json_state
        # Notify subscribers about the changed value.
        self.push_change_event('commandState', json_state)
        self.push_archive_event('commandState', json_state)

    async def update_randomNumber(self) -> None:
        '''
        This coroutine frequently updates the randomNumber
        attribute.
        '''
        while self._thread_run:
            # Run after self._updateSleep seconds.
            await asyncio.sleep(self._updateSleep)
            # Create a new random number.
            self._my_ro_attribute = random()
            # Pushed the change and archive events.
            self.push_change_event('randomNumber', self._randomNumber)
            self.push_archive_event('randomNumber', self._randomNumber)
        self.info_stream(f'{now()} The update_randomNumber coroutine has been stopped. Good bye.')


    async def execute_long_running_command(self, command_id: DevString = None, timeOut: DevDouble = None) -> None:
        '''
        Coroutine that resembles the busy part of a long running
        command.
        '''
        # Create my own pseudo state.
        # For simplicity later I use the timestamp as a key.
        myState = {'command_id': command_id, 'timestamp': now(), 'state': 'running'}
        # Now update the commandState attribute. This will also send
        # events.
        await self.update_commandState(myState)

        # Now pretend to do something while yielding the CPU.
        self.info_stream(f'{now()} I am the \"{myState["command_id"]}\" command and my state is now \"{myState}\". I will pretend to be busy for {self._waitTime}s before changing my state to \"done\"...')
        await asyncio.sleep(self._waitTime)
        # Assign a different state.
        myState = {'command_id': command_id, 'timestamp': now(), 'state': 'done'}
        # And update the commandState attribute and send a
        # change event again.
        await self.update_commandState(myState)
        self.info_stream(f'{now()} I am the \"{myState["command_id"]}\" command and my state is now \"{myState}\". This means that I am done. Good bye.')


    @command(dtype_in = DevString, dtype_out = DevString)
    async def longRunningCommand(self, command_id: DevString = None) -> DevString:
        '''
        This is the entry point for long running commands.
        '''
        task = asyncio.create_task(self.execute_long_running_command(command_id))
        return f'Executing long running command "{command_id}."'


    @command(dtype_in = DevDouble, dtype_out = DevVoid)
    async def wait_cmd(self, sleepTime) -> DevVoid:
        '''
        A command that blocks the TC event loop in the normal
        green mode for sleepTime seconds. This would block all
        clients from accessing this device. In asyncio green mode
        the device continues to work normally because the device is
        now responsible for the proper synchronisation of internal
        data.
        When you run this command in "normal" client mode in iTango,
        then an exception will be thrown after the normal CORBA
        timeout:

        In [16]: dp.wait_cmd(10)
API_CorbaException: TRANSIENT CORBA system exception: TRANSIENT_CallTimedout
(For more detailed information type: tango_error)

        Setting the client to asyncio green mode like so:

        dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
        dp.set_green_mode(tango.GreenMode.Asyncio)

        and calling the command again will not change anything. The same
        CORBA timeout will happen.
        '''
        self.info_stream(f'{now()} This long running command will start now and wait for {sleepTime}s before returning...')
        await asyncio.sleep(sleepTime)
        self.info_stream(f'{now()} This long running command waited for {sleepTime}s and is now done. Good bye.')


    @attribute(dtype = float)
    async def waitTime(self) -> DevDouble:
        '''
        The time that the randomNumber update coroutine will sleep
        between runs.
        '''
        return self._waitTime
    @waitTime.write
    async def waitTime(self, waitTime: DevDouble = None) -> None:
        self._waitTime = waitTime

    @attribute(dtype = str)
    async def commandState(self) -> DevString:
        '''
        The state of running and finished commands.
        '''
        return self._commandState

    @attribute(dtype = float, abs_change = 0.1)
    async def my_ro_attribute(self) -> DevDouble:
        '''
        A random number that is frequently modified.
        '''
        return self._my_ro_attribute


if __name__ == '__main__':
    run((AsyncLongRunningCommand, ), green_mode = GreenMode.Asyncio)
    print(f'\n\tTerminated at {now()}')
