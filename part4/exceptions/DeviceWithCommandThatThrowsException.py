'''
Run this Python3 script on your local machine like this:
python3 DeviceWithCommandThatThrowsException.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''
from tango import (
    DevState,
    Except
)
from tango.server import (
    Device,
    command,
    run
)

class DeviceWithCommandThatThrowsException(Device):
    def init_device(self) -> None:
        super().init_device()
        self.set_state(DevState.ON)

    @command()
    def throw_exception(self) -> None:
        # This one is simple: Just throw an exception.
        # It is a bit trickier for clients:
        # They will need to catch a DevFailed object first.
        # The DevFailed object is a container for DevError objects
        # and represents the exception stack. In order to see which
        # exceptions are packed up in the DevFailed object, a client
        # has to iterate over all elements in the DevFailed.args list:
        # try:
        #    dp.raise_exception()
        # except tango.DevFailed as e:
        #    print(f'The exception that the function raised:\n{e.args[0]}\nThe exception that Tango added because the command failed:\n{e.args[1]}')
        self.info_stream('Will throw an exception now...')
        Except.throw_exception('DeviceCommandWithException_This_is_a_reason', 'A wonderful description of this exception that let\'s users know why this happened.', 'DeviceCommandWithException.throw_exception()')

    @command()
    def rethrow_exception(self) -> None:
        # Generate and then re-throw a Tango DevFailed exception.
        # The same logic as above applies for clients.
        self.info_stream('Causing an exception...')
        dev = 'foo/bar/2'
        try:
            dp = DeviceProxy(dev)
            dp.ping()
        except DevFailed as ex:
            self.info_stream(f'Caught the exception {ex}.')
            self.info_stream('Re-reaising it with additional information...')
            Except.re_throw_exception(
            ex,
            f'CannotPingDeviceEx', # Reason
            f'Tried to ping device \"{dev}\" but failed. Cannot continue.', # Description
            f'DeviceWithExceptions.rethrow_exception instance {self.get_name()}' # Origin
        )


def main(args = None, **kwargs):
    return run((DeviceWithCommandThatThrowsException,), **kwargs)

if __name__ == '__main__':
    main()
