'''
Run this Python3 script on your local machine like this:
python3 DeviceWithPolledCommand.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''

from tango import (
    DevState
)
from tango.server import (
    Device,
    attribute,
    command,
    run
)

from numpy import random

class DeviceWithPolledCommand(Device):
    def init_device(self):
        super().init_device()
        self.set_state(DevState.ON)

    @command(polling_period = 100)
    def dostuff(self) -> float:
        return random.random()


def main(args = None, **kwargs):
    return run((DeviceWithPolledCommand,), **kwargs)

if __name__ == '__main__':
    main()
