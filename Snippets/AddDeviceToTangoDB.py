from tango import (
    Database,
    DbDevInfo
)

def main(args=None, **kwargs) -> int:
    # Connect to the TangoDB
    db = Database()
    # Create a new and empty DbDevInfo object.
    dev_info = DbDevInfo()
    # Now fill it with content.
    # name is the device's TRL or full TRL.
    dev_info.name = 'my/own/device'
    # The class is the class that we implemented.
    dev_info._class = 'MyDevice'
    # This is the DeviceServer name and instance:
    #    DeviceServer_name/instance
    # The name is the Python script that you will execute and which needs to
    # contain the class that you specified above.
    # You can choose the instance freely as long as the string
    # only contains ASCII characters [A-z0-1_].
    dev_info.server = 'test_device/MyDevice'
    # Now we can tell TangoDB to add the device. If TangoDB cannot add it,
    # it will raise an exception.
    db.add_device(dev_info)
    # That's it. Good bye!

if __name__ == '__main__':
    main()
