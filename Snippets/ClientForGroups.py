# Create a new group
group = tango.group.Group('my_group')

# Now add devices to the group
group.add('sys/tg_test/*')
# Without a TangoDB one has to specify a full TRL plus "#dbase=no"
# Like this:
group.add(('tango://127.0.0.1:45679/foo/bar/2#dbase=no', 'tango://127.0.0.1:45678/foo/bar/1#dbase=no'))

# Check if a certain device is a member of the group
group.contains('sys/tg_test/arm3')
#
# Or just get the entire device list in one go
group.get_device_list()

#Out[9]: ['sys/tg_test/1', 'sys/tg_test/arm1', 'sys/tg_test/arm3', 'sys/tg_test/fisch', 'sys/tg_test/mac'

# Do something
result = group.command_inout('status')

# A call could simply fail because a device did not implement the command. The
# same is true for attributes! In such a case a result object's has_failed()
# method would return False for that device. This would also fail for a
# device that is defined in the TangoDB but not running.
# One cannot tell from the has_failed() result why an operation failed. For that
# one would have to look at what the result's get_err_stack() method returned.
[ print(f'Device {i.dev_name()} failed:\n{i.get_err_stack()}') for i in result if i.has_failed() == True ]

# Example output
# In [58]: [ print(f'Device {i.dev_name()} failed:\n{i.get_err_stack()}') for i in result if i.has_failed() == True ]
# Device sys/tg_test/1 failed:
# (DevError(desc = 'Device sys/tg_test/1 is not exported (hint: try starting the device server)', origin = 'virtual std::string Tango::DeviceProxy::get_corba_name(bool) at (/Users/thomas.juerges/workspace.thomas/tango/build_tango/build/tango.main/cppTango/src/client/devapi_base.cpp:2422)', reason = 'API_DeviceNotExported', severity = tango._tango.ErrSeverity.ERR), DevError(desc = 'Failed to execute command_inout on device sys/tg_test/1, command status', origin = 'virtual long Tango::Connection::command_inout_asynch(const std::string &, const DeviceData &, bool) at (/Users/thomas.juerges/workspace.thomas/tango/build_tango/build/tango.main/cppTango/src/client/proxy_asyn.cpp:72)', reason = 'API_CommandFailed', severity = tango._tango.ErrSeverity.ERR))
# Device sys/tg_test/arm3 failed:
# (DevError(desc = 'Device sys/tg_test/arm3 is not exported (hint: try starting the device server)', origin = 'virtual std::string Tango::DeviceProxy::get_corba_name(bool) at (/Users/thomas.juerges/workspace.thomas/tango/build_tango/build/tango.main/cppTango/src/client/devapi_base.cpp:2422)', reason = 'API_DeviceNotExported', severity = tango._tango.ErrSeverity.ERR), DevError(desc = 'Failed to execute command_inout on device sys/tg_test/arm3, command status', origin = 'virtual long Tango::Connection::command_inout_asynch(const std::string &, const DeviceData &, bool) at (/Users/thomas.juerges/workspace.thomas/tango/build_tango/build/tango.main/cppTango/src/client/proxy_asyn.cpp:72)', reason = 'API_CommandFailed', severity = tango._tango.ErrSeverity.ERR))
# Device sys/tg_test/mac failed:
# (DevError(desc = 'TRANSIENT CORBA system exception: TRANSIENT_CallTimedout', origin = 'void Tango::Connection::connect(const std::string &) at (/Users/thomas.juerges/workspace.thomas/tango/build_tango/build/tango.main/cppTango/src/client/devapi_base.cpp:631)', reason = 'API_CorbaException', severity = tango._tango.ErrSeverity.ERR), DevError(desc = 'Failed to connect to device sys/tg_test/mac', origin = 'void Tango::Connection::connect(const std::string &) at (/Users/thomas.juerges/workspace.thomas/tango/build_tango/build/tango.main/cppTango/src/client/devapi_base.cpp:631)', reason = 'API_CantConnectToDevice', severity = tango._tango.ErrSeverity.ERR), DevError(desc = 'Failed to execute command_inout on device sys/tg_test/mac, command status', origin = 'virtual long Tango::Connection::command_inout_asynch(const std::string &, const DeviceData &, bool) at (/Users/thomas.juerges/workspace.thomas/tango/build_tango/build/tango.main/cppTango/src/client/proxy_asyn.cpp:72)', reason = 'API_CommandFailed', severity = tango._tango.ErrSeverity.ERR))
# Out[58]: [None, None, None]
# The last line in the output is an artefact.

# Print the values that were returned
[ print(f'Device {i.dev_name()} command {i.obj_name()} returned value={i.get_data()}\n') for i in result]
# Example output
# In [57]: [ print(f'{i.dev_name()}={i.get_data()}') for i in result ]
# sys/tg_test/1=None
# sys/tg_test/arm1=The device is in RUNNING state.
# sys/tg_test/arm3=None
# sys/tg_test/fisch=The device is in RUNNING state.
# sys/tg_test/mac=None

# Reading of attributes
result = group.read_attribute('double_scalar')
# Print all values
[ print(f'Attribute {i.obj_name()} value of device {i.dev_name()}={i.get_data().value}\n') for i in result]
# Example output:
# In [61]: [ print(f'Attribute {i.obj_name()} value of device {i.dev_name()}={i.get_data().value}') for i in result]
# Attribute double_scalar value of device sys/tg_test/1=None
# Attribute double_scalar value of device sys/tg_test/arm1=44.45456531461067
# Attribute double_scalar value of device sys/tg_test/arm3=None
# Attribute double_scalar value of device sys/tg_test/fisch=247.27693827122883
# Attribute double_scalar value of device sys/tg_test/mac=None
# Out[61]: [None, None, None, None, None]
# The last line in the output is an artefact.

# Writing of attributes
result = group.write_attribute('double_scalar_w', 1.0)
# Check if writing was successful
[ print(f'Writing attribute value failed? Device {i.dev_name()}={i.has_failed()}') for i in result ]
# Example output
# In [9]: [ print(f'Writing attribute value failed? Device {i.dev_name()}={i.has_failed()}') for i in result ]
# Writing attribute value failed? Device sys/tg_test/1=True
# Writing attribute value failed? Device sys/tg_test/arm1=False
# Writing attribute value failed? Device sys/tg_test/arm3=True
# Writing attribute value failed? Device sys/tg_test/fisch=False
# Writing attribute value failed? Device sys/tg_test/mac=True
# Out[9]: [None, None, None, None, None]
# The last line in the output is an artefact.

# Read back the written values
result = group.read_attribute('double_scalar_w')

# Print the values
[ print(f'Attribute {i.obj_name()} value of device {i.dev_name()}={i.get_data().value}') for i in result]
# Example output
# In [11]: [ print(f'Attribute {i.obj_name()} value of device {i.dev_name()}={i.get_data().value}') for i in result]
# Attribute double_scalar_w value of device sys/tg_test/1=None
# Attribute double_scalar_w value of device sys/tg_test/arm1=1.0
# Attribute double_scalar_w value of device sys/tg_test/arm3=None
# Attribute double_scalar_w value of device sys/tg_test/fisch=1.0
# Attribute double_scalar_w value of device sys/tg_test/mac=None
# Out[11]: [None, None, None, None, None]
# The last line in the output is an artefact.

# A goup of heterogenous devices
group = tango.group.Group('my_group')
group.add('sys/tg_test/*')
group.add(('tango://127.0.0.1:45679/foo/bar/2#dbase=no', 'tango://127.0.0.1:45678/foo/bar/1#dbase=no'))
group.get_device_list()
# Example output
# Out[19]: ['sys/tg_test/1', 'sys/tg_test/arm1', 'sys/tg_test/arm3', 'sys/tg_test/fisch', 'sys/tg_test/mac', 'tango://127.0.0.1:45679/foo/bar/2#dbase=no', 'tango://127.0.0.1:45678/foo/bar/1#dbase=no']

# Read an attribute. It might be that this attribute is not available on all
# devices.
result = group.read_attribute('my_ro_attribute')
# Check the values.
[ print(f'Attribute {i.obj_name()} value of device {i.dev_name()}={i.get_data().value}') for i in result]
# Example output
# Attribute my_ro_attribute value of device sys/tg_test/1=None
# Attribute my_ro_attribute value of device sys/tg_test/arm1=None
# Attribute my_ro_attribute value of device sys/tg_test/arm3=None
# Attribute my_ro_attribute value of device sys/tg_test/fisch=None
# Attribute my_ro_attribute value of device sys/tg_test/mac=None
# Attribute my_ro_attribute value of device tango://127.0.0.1:45679/foo/bar/2#dbase=no=1.2345
# Attribute my_ro_attribute value of device tango://127.0.0.1:45678/foo/bar/1#dbase=no=1.2345
# Out[21]: [None, None, None, None, None, None, None]
# The last line in the output is an artefact.
