'''
Run this Python3 script on your local machine like this:
python3 SimplestSKATangoDevice.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''

from tango import (
    DevState
)
from tango.server import (
    run
)
from ska_tango_base import (
    SKABaseDevice
)

class CM():
    max_queued_tasks = 0
    max_executing_tasks = 1

class SimplestSKATangoDevice(SKABaseDevice):
    def create_component_manager(self):
        return CM()

    def init_device(self):
        super().init_device()
        self.set_state(DevState.ON)

    def delete_device(self):
        self.set_state(DevState.OFF)
        super().delete_device()


def main(args = None, **kwargs):
    return run((SimplestSKATangoDevice,), **kwargs)

if __name__ == '__main__':
    main()
