'''
Run this Python3 script on your local machine like this:
python3 DeviceWithAlarmsAndState.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''
from tango import (
    DevState
)
from tango.server import (
    Device,
    attribute,
    command,
    run
)

class DeviceWithAlarmsAndState(Device):
    def push_events(self, attr: str, value) -> None:
        self.push_change_event(attr, value)
        self.push_archive_event(attr, value)
        # New in cppTango 10.0.0
        self.push_alarm_event(attr, value)

    def init_device(self):
        super().init_device()
        # Set the initial value of my read-only-attribute
        self._my_ro_attribute_value = 1.2345
        # Set the initial value of my read-write-attribute
        self._my_rw_attribute_value = 5.4321

        for attr in ('my_ro_attribute', 'my_rw_attribute'):
            self.set_archive_event(attr, True, True)
            self.set_change_event(attr, True, True)
            # New in cppTango 10.0.0
            self.set_alarm_event(attr, True)

        self.set_state(DevState.ON)

    # This attribute has its minimum difference for a change event set to 0.1.
    @attribute(archive_abs_change = 0.75, abs_change = 0.1, min_alarm = -100, max_alarm = 150)
    def my_ro_attribute(self) -> float:
        return self._my_ro_attribute_value

    @command()
    def modify_ro_attribute_value(self, new_value: float) -> None:
        self._my_ro_attribute_value = new_value
        self.push_events('my_ro_attribute', new_value)

    # This attribute has its minimum difference for a change event set to 0.01.
    @attribute(archive_abs_change = 0.5, abs_change = 0.01, min_alarm = -1.234, min_warning = -0.1, max_warning = 10.0, max_alarm = 11)
    def my_rw_attribute(self) -> float:
        return self._my_rw_attribute_value
    @my_rw_attribute.write
    def my_rw_attribute(self, new_value: float = None) -> None:
        self._my_rw_attribute_value = new_value
        self.push_events('my_rw_attribute', new_value)

    @command()
    def set_device_state(self, state: DevState) -> None:
        self.set_state(state)

def main(args = None, **kwargs):
    return run((DeviceWithAlarmsAndState,), **kwargs)

if __name__ == '__main__':
    main()
