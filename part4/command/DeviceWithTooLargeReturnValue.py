'''
Run this Python3 script on your local machine like this:
python3 DeviceWithTooLargeReturnValue.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')

Now I just run two loops to probe the maximum allowed size of a returned CORBA
object:

def gauge_limit():
    # Find the roughly lower boundary when a CORBA object is too big.
    bit_number = 0
    for bit_number in range(0, 32):
        try:
            a = dp.call_me(2**bit_number)
            bit_number += 1
        except:
            print(f'Failed at {2**bit_number} = {hex(2**bit_number)}')
            break
    # Now reduce the size by one byte at a time to find the exact limit.
    for i in range(2**bit_number, 2**(bit_number - 1), -1):
        try:
            a = dp.call_me(i)
            print(f'Success with {i} = {hex(i)}')
            break
        except:
            pass

gauge_limit()
Success with 268435419 = 0xfffffdb'''

from tango import (
    DevState
)
from tango.server import (
    command,
    Device,
    run
)

class DeviceWithTooLargeReturnValue(Device):
    def init_device(self):
        super().init_device()
        self.set_state(DevState.ON)

    @command()
    def call_me(self, size: int) -> str:
        s = "Yo!"
        returned_string = str("").ljust(size - len(s)) + s
        print(f'Size of string = {len(returned_string)}')
        return returned_string

def main(args = None, **kwargs):
    return run((DeviceWithTooLargeReturnValue,), **kwargs)

if __name__ == '__main__':
    main()
