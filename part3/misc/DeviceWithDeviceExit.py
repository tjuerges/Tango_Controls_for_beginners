'''
This example demonstrates that the signal handler that cppTango has installed will
make sure that device_exit is called.

Run this Python3 script on your local machine like this:
python3 DeviceWithDeviceExit.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then stop the Device by sending it a TERM signal like so:
pkill -SIGTERM -lf DeviceWithDeviceExit.py
'''

from tango import (
    DevBoolean,
    DevDouble,
    DevState,
    DevVoid,
    EnsureOmniThread
)
from tango.server import (
    Device,
    attribute,
    command,
    run
)

import threading, time

class DeviceWithDeviceExit(Device):
    def init_device(self):
        super().init_device()
        # Set the initial value of my read-only-attribute
        self.__my_ro_attribute_value = 1.2345
        # Set the initial value of my read-write-attribute
        self.__my_rw_attribute_value = 5.4321

        self.set_archive_event('my_ro_attribute', True, True)
        self.set_archive_event('my_rw_attribute', True, True)
        self.set_change_event('my_ro_attribute', True, True)
        self.set_change_event('my_rw_attribute', True, False)

        self.set_state(DevState.ON)

        self.stop = False
        self.thread = threading.Thread(target = self.push_event_thread)
        self.thread.start()

    def delete_device(self):
        self.set_state(DevState.OFF)
        self.stop = True
        # This sleep is necessary to give the omniThread a moment to stop.
        # Otherwise pressing CTRL-C to stop this DeviceServer results in a
        # segfault.
        time.sleep(1.1)
        self.info_stream('This device will now shut down. Thanks and good bye.')
        super().delete_device()

    def push_event_thread(self):
        with EnsureOmniThread():
            while self.stop is False:
                time.sleep(1.0)
                self.info_stream('Pushing events...')
                self.push_archive_event('my_ro_attribute', self.__my_ro_attribute_value)
                self.push_archive_event('my_rw_attribute', self.__my_rw_attribute_value)
                self.push_change_event('my_ro_attribute', self.__my_ro_attribute_value)
                self.push_change_event('my_rw_attribute', self.__my_rw_attribute_value)
        self.info_stream('Detected that this push_event_thread should now stop. Thanks and good bye!')


    # This attribute has its minimum difference for a change event set to 0.1.
    @attribute(dtype = DevDouble, archive_abs_change = 0.75, abs_change = 0.1)
    def my_ro_attribute(self) -> DevDouble:
        return self.__my_ro_attribute_value

    # This attribute has its minimum differencefor a change event set to 0.01.
    @attribute(dtype = DevDouble, archive_abs_change = 0.5, abs_change = 0.01)
    def my_rw_attribute(self) -> DevDouble:
        return self.__my_rw_attribute_value
    @my_rw_attribute.write
    def my_rw_attribute(self, value: DevDouble = None) -> DevVoid:
        self.__my_rw_attribute_value = value
        self.push_archive_event('my_rw_attribute', value)
        self.push_change_event('my_rw_attribute', value)

def main(args = None, **kwargs):
    return run((DeviceWithDeviceExit,), **kwargs)

if __name__ == '__main__':
    main()
