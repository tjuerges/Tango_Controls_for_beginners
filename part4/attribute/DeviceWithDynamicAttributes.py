'''
Run this Python3 script on your local machine like this:
python3 DeviceWithDynamicAttributes.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''
from tango import (
    AttrWriteType,
    DevDouble,
    DevFailed,
    DevState,
    DevString,
    Except,
    EnsureOmniThread,
)
from tango.server import (
    Device,
    attribute,
    command,
    run
)
from logging import (
    basicConfig,
    DEBUG,
    INFO,
    getLogger,
)
from json import (
    dumps,
    loads
)
from datetime import (
    datetime,
    UTC
)
from threading import (
    Lock,
    Thread
)
from numpy.random import random
from time import sleep
from sys import exit


basicConfig(level = INFO)
logger = getLogger('DeviceWithDynamicAttributes')


class DeviceWithDynamicAttributes(Device):
    def init_device(self):
        super().init_device()
        self.__thread_run = True
        self.__updateSleep = 1.0
        self.__values = {}
        self.__access_lock = Lock()
        self.__thread = Thread(name = 'update_attribute_values', target = self.update_attribute_values)
        self.__thread.start()
        self.set_state(DevState.ON)

    def delete_device(self):
        self.__thread_run = False
        self.__thread.join()

    def update_attribute_values(self) -> None:
        '''
        This thread frequently updates the randomNumber
        attribute.
        '''
        with EnsureOmniThread():
            while self.__thread_run:
                try:
                    # Run after self._updateSleep seconds.
                    sleep(self.__updateSleep)
                    # Create a new random number for each attribute.
                    while self.__access_lock.acquire(blocking = True, timeout = 0.5) is False:
                        logger.debug(f'{self.__thread.name} is waiting for the access lock...')
                        sleep(0.5)
                    for k in self.__values.keys():
                        value = random()
                        self.__values[k] = value
                        logger.debug(f'Attribute {k} = {value}')
                        # Push the change and archive events.
                        self.push_change_event(k, value)
                        self.push_archive_event(k, value)
                except DevFailed as e:
                    Except.print_exception(e)
                    Except.re_throw_exception(e)
                finally:
                    self.__access_lock.release()
                    logger.debug(f'{self.__thread.name} released the lock.')
        logger.debug(f'The {self.__thread.name} thread has been stopped. Good bye.')

    @command
    def addDynamicAttribute(self, _attribute_conf: str = None) -> str:
        '''
        Create a dynamic attribute.
        _attribute_conf: This is a dict marshalled as a JSON string.
        The dict has to have to following entries:
            - name: Name of the attribute to be created.
            - read_write: Either READ or READ_WRITE to define a
              read-only or read-write attribute.
            - abs_change: The abs_change value for CHANGE_EVENTS.
        '''
        attribute_conf = loads(_attribute_conf)
        try:
            while self.__access_lock.acquire(blocking = True, timeout = 0.1) is False:

                logger.debug(f'{__name__} is waiting for the access lock...')
                sleep(0.1)
            if attribute_conf['name'] not in self.__values:
                # Create an attribute object.
                # Retrieve all essential data from the JSON string.
                attr = attribute(
                    name=attribute_conf['name'],
                    dtype=float,
                    access=getattr(AttrWriteType, attribute_conf['read_write']),
                    fget=self.generic_read,
                    fset=self.generic_write,
                    abs_change = attribute_conf['abs_change'],
                )
                # Initialise the value and add the attribute entry
                # to the local dict.
                self.__values[attribute_conf['name']] = random()
                # Now add the attribute to the device.
                self.add_attribute(attr)
                # Enable manual pushing of events. Just for fun.
                self.set_change_event(attribute_conf['name'], True, True)
                self.set_archive_event(attribute_conf['name'], True, True)
                logger.debug(f'{__name__} Added dynamic attribute {attribute_conf['name']}.')
                # Return to the client the current dict of attributes
                # as a JSON string.
                return dumps(self.__values)
            else:
                self.__access_lock.release()
                Except.throw_exception('Attribute already exists.', f'An attribute with the name {attribute_conf['name']} already exists. I will not create another one.', 'addDynamicAttribute')
        except DevFailed as e:
            Except.print_exception(e)
            Except.re_throw_exception(e)
        except Exception as e:
            logger.error(f'{e}')
            exit(1)
        finally:
            logger.debug(f'{__name__} Releasing the access lock.')
            self.__access_lock.release()

    @command
    def removeDynamicAttribute(self, attribute_name: str = None) -> str:
        try:
            while self.__access_lock.acquire(blocking = True, timeout = 0.1) is False:
                logger.debug(f'{__name__} Waiting for the access lock...')
                sleep(0.1)
            if attribute_name in self.__values:
                # Remove the attribute from the device.
                # Attention!
                # When running in nodb mode, the parameter
                # clean_db (default is True) must be set to False!
                self.remove_attribute(attribute_name, free_it = True, clean_db = False)
                self.__values.pop(attribute_name)
                logger.debug(f'Removed dynamic attribute {attribute_name}.')
                return dumps(self.__values)
            else:
                Except.throw_exception('Attribute does not exist.', f'An attribute with the name {attribute_name} does not exist. I cannot remove it.', 'removeDynamicAttribute')
        finally:
            logger.debug(f'{__name__} Releasing the access lock.')
            self.__access_lock.release()

    def generic_read(self, attr):
        attr_name = attr.get_name()
        logger.debug(f'{self.__values}')
        value = self.__values[attr_name]
        logger.debug(f'Reading attribute {attr_name}, value = {value}.')
        attr.set_value(value)

    def generic_write(self, attr):
        attr_name = attr.get_name()
        value = attr.get_write_value()
        logger.debug(f'Writing attribute {attr_name} = {value}')
        while self.__access_lock.acquire(blocking = True, timeout = 0.1) is False:
            logger.debug(f'{__name__} Waiting for the access lock...')
        self.__values[attr.get_name()] = value
        self.__access_lock.release()

def main(args = None, **kwargs):
    return run((DeviceWithDynamicAttributes,), **kwargs)

if __name__ == '__main__':
    main()
