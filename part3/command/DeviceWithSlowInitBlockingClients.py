'''
Run this Python3 script on your local machine like this:
python3 DeviceWithSlowInitBlockingClients.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''
from tango import (
    DevState,
)
from tango.server import (
    Device,
    run
)

from time import (
    sleep
)

class DeviceWithSlowInitBlockingClient(Device):
    def init_device(self):
        # Demonstrate how a very slow init_device function affects
        # clients.
        # - With a TangoDB clients will catch an API_DeviceNotExported
        #   exception.
        # - Without a TangoDB clients will receive an API_CorbaException
        #   exception in the exception stack at position 0.
        super().init_device()
        self.set_state(DevState.ON)
        # Make certain that this takes longer than the standard client
        # timeout. Therefore set it to much longer than 3s.
        wait_time = 15.0
        self.info_stream(f'Blocking "init_device" for {wait_time}...')
        sleep(wait_time)
        self.info_stream(f'Blocking "init_device" for {wait_time} done. Exiting "init_device" now.')

def main(args = None, **kwargs):
    return run((DeviceWithSlowInitBlockingClient,), **kwargs)

if __name__ == '__main__':
    main()
