'''
Run this Python3 script on your local machine like this:
python3 DeviceWithOwnPythonEventThread.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''
from tango import (
    DevFailed,
    DevState,
    EnsureOmniThread
)

from tango.server import (
    Device,
    attribute,
    command,
    run
)

from time import (
    sleep
)

from threading import (
    Thread
)

class DeviceWithOwnPythonEventThread(Device):
    def init_device(self):
        super().init_device()
        # Set the initial value of my ro and rw attributes. I keep them in a
        # dict.
        self._attrs = {}
        self._attrs["my_ro_attribute"] = 1.2345
        self._attrs["my_rw_attribute"] = 5.4321
        for attr in self._attrs.keys():
            self.set_archive_event(attr, True, True)
            self.set_change_event(attr, True, True)
        # The cycle time of the event loop is configurable.
        self._delay = 1.0
        self.set_state(DevState.ON)

        self.stop = False
        self.thread = Thread(target = self._push_event_thread)
        self.thread.start()

    def delete_device(self):
        self.set_state(DevState.OFF)
        self.stop = True
        # This sleep is necessary to give the omniThread a moment to stop.
        # Otherwise pressing CTRL-C to stop this DeviceServer results in a
        # segfault.
        sleep(self._delay + 1.0)

    def _push_event_thread(self):
        with EnsureOmniThread():
            while self.stop is False:
                sleep(self._delay)
                self.debug_stream('Pushing events...')
                for attr, value in self._attrs.items():
                    self.push_change_event(attr, value)
                    self.push_archive_event(attr, value)

    # The delay in the event loop can be modified by setting this attribute's
    # value.
    @attribute
    def delay(self) -> float:
        return self._delay
    @delay.write
    def delay(self, value: float) -> None:
        self._delay = value

    # This attribute has its minimum difference for a change event set to 0.1.
    @attribute(archive_abs_change = 0.75, abs_change = 0.1)
    def my_ro_attribute(self) -> float:
        return self._attr["my_ro_attribute"]

    # This attribute has its minimum differencefor a change event set to 0.01.
    @attribute(archive_abs_change = 0.5, abs_change = 0.01)
    def my_rw_attribute(self) -> float:
        return self._attr["my_rw_attribute"]
    @my_rw_attribute.write
    def my_rw_attribute(self, value: float = None) -> None:
        attr = "my_rw_attribute"
        self._attr[attr] = new_value
        self.push_archive_event(attr, new_value)
        self.push_change_event(attr, new_value)

    @command
    def modify_ro_attribute_value(self, new_value: float) -> None:
        attr = "my_ro_attribute"
        self._attr[attr] = new_value
        self.push_archive_event(attr, new_value)
        self.push_change_event(attr, new_value)

    @command
    def toggle_change_event_check(self, value: bool) -> None:
        for attr in self._attrs.keys():
            self.set_change_event(attr, True, value)

def main(args = None, **kwargs):
    return run((DeviceWithOwnPythonEventThread,), **kwargs)

if __name__ == '__main__':
    main()
