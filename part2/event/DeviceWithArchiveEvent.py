'''
Run this Python3 script on your local machine like this:
python3 DeviceWithArchiveEvent.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''
from tango import (
    DevState
)
from tango.server import (
    Device,
    attribute,
    command,
    run
)

class DeviceWithArchiveEvent(Device):
    def init_device(self):
        super().init_device()
        # Set the initial value of my read-only-attribute
        self.__my_ro_attribute_value = 1.2345
        # Set the initial value of my read-write-attribute
        self.__my_rw_attribute_value = 5.4321

        # Enable pushing of archive events for both attributes
        # 1st parameter: Attribute name
        # 2nd parameter: Set to True if events are pushed manually, i.e.
        # by calling
        # self.push_archive_event(my_ro_attribute', new_value)
        # or if the Device relies on the DeviceServer's polling loop to
        # send events.
        # 3rd parameter: When True, the criteria specified for the archive
        # event are verified and the event is only pushed if they are
        # met. If set to False, the event is sent without any value checking.
        # Note that the DeviceServer polling loop is unsuitable for Devices
        # with more than just a couple of plain type Attributes.
        self.set_archive_event('my_ro_attribute', True, True)
        self.set_archive_event('my_rw_attribute', True, True)

        self.set_state(DevState.ON)

    # This attribute has its minimum difference for an archive change event set
    # to 0.1.
    @attribute(archive_abs_change = 0.1)
    def my_ro_attribute(self) -> float:
        return self.__my_ro_attribute_value

    # Allow a client to modify the value of our read-only attribute.
    @command
    def modify_ro_attribute_value(self, new_value: float) -> None:
        self.__my_ro_attribute_value = new_value
        self.push_archive_event('my_ro_attribute', new_value)

    # This attribute has its minimum differencefor an archive change event set
    # to 0.01.
    @attribute(archive_abs_change = 0.01)
    def my_rw_attribute(self) -> float:
        return self.__my_rw_attribute_value
    @my_rw_attribute.write
    def my_rw_attribute(self, value: float = None) -> None:
        self.__my_rw_attribute_value = value
        self.push_archive_event('my_rw_attribute', value)

    # This attribute has its minimum difference for an archive change event set
    # to 0.01.
    @attribute(archive_rel_change = 0.5, archive_period = 7000, polling_period = 3000)
    def my_rw_attribute(self) -> float:
        return self.__my_rw_attribute_value
    @my_rw_attribute.write
    def my_rw_attribute(self, value: float = None) -> None:
        self.__my_rw_attribute_value = value
        self.push_archive_event('my_rw_attribute', value)


def main(args = None, **kwargs):
    return run((DeviceWithArchiveEvent,), **kwargs)

if __name__ == '__main__':
    main()
