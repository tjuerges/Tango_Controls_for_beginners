'''
Run this Python3 script on your local machine like this:
python3 DeviceWithROAttribute.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''
from tango import (
    DevDouble,
    DevState,
    DevVoid
)
from tango.server import (
    Device,
    attribute,
    run
)

class DeviceWithROAttribute(Device):
    def init_device(self):
        super().init_device()
        # Set the initial value of my read-only-attribute
        self.__my_ro_attribute_value = 1.2345
        self.set_state(DevState.ON)

    @attribute(dtype = DevDouble)
    def my_ro_attribute(self) -> DevDouble:
        return self.__my_ro_attribute_value

    # Type hints for native Python types are experimentally supported since PyTango 9.5.0:
    # Note: One can use both: list[...] or tuple[...] in type hints.
    @attribute
    def my_ro_type_hint_attribute(self) -> float:
        return self.__my_ro_attribute_value

def main(args = None, **kwargs):
    return run((DeviceWithROAttribute,), **kwargs)

if __name__ == '__main__':
    main()
