'''
Run this Python3 script on your local machine like this:
python3 d1.py test -v4 -nodb -port 50000 -dlist buttons/d1/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:50000/buttons/d1/1#dbase=no')
'''
from tango import (
    DevFailed,
    DeviceProxy,
    DevState,
    EventData,
    EventType,
)

from tango.exception import (
    Except
)

from tango.server import (
    Device,
    attribute,
    command,
    run
)


class Buttons_device(Device):
    def connect_device(self, dev) -> DeviceProxy:
        if dev == self._d1:
            port = 50000
        else:
            port = 50001
        return DeviceProxy(f'tango://127.0.0.1:{port}/{dev}#dbase=no')

    def init_device(self) -> None:
        super().init_device()
        self._d1 = 'buttons/d1/1'
        self._d2 = 'buttons/d2/1'
        self._my_name = self.get_name()
        self.info_stream(f'Hello world! This is {self._my_name}.')
        self._attr1 = 1.2345
        self._attr2 = 5.4321

        self.set_change_event('attr1', True, True)
        self.set_archive_event('attr1', True, True)
        self.set_change_event('attr2', True, True)
        self.set_archive_event('attr2', True, True)

        self.change_subscription_id = None
        self.archive_subscription_id = None

        self.set_state(DevState.ON)

    def delete_device(self) -> None:
        try:
            if self.change_subscription_id is not None:
                self._dp.unsubscribe_event(self.change_subscription_id)
            if self.archive_subscription_id is not None:
                self._dp.unsubscribe_event(self.archive_subscription_id)
        except DevFailed as ex:
            Except.print_exception(ex)
        except Exception as ex:
            self.info_stream(f'{ex}')
        self.set_state(DevState.OFF)
        self.info_stream(f'{self._my_name} says good bye.')

    def cb_change_event(self, event: EventData) -> None:
        # D1.attr1 is set to D2.attr1
        # D2.attr2 is set to D1.attr2
        if event.err is False:
            self.info_stream(f'Received from {event.device} {event.attr_value.value}')
            if event.device.dev_name() == self._d1:
                self._attr2 = event.attr_value.value
            elif event.device.dev_name() == self._d2:
                self._attr1 = event.attr_value.value
            else:
                breakpoint()
                self.info_stream(f'Cannot determine from which device the event came! Event:\n{event}')
            self.info_stream(f'{self._my_name} attr1={self._attr1} attr2={self._attr2}')
        else:
            self.info_stream(f'Received an error event on device {self._my_name} when a CHANGE_EVENT was expected. The full content of the error event is:\n{event}\nWill ignore the error and continue with normal operation.')

    @attribute(archive_abs_change = 0.01, abs_change = 0.01)
    def attr1(self) -> float:
        return self._attr1
    @attr1.write
    def attr1(self, value: float) -> None:
        self._attr1 = value
        self.push_change_event('attr1', value)
        self.push_archive_event('attr1', value)

    @attribute(archive_abs_change = 0.01, abs_change = 0.01)
    def attr2(self) -> float:
        return self._attr2
    @attr2.write
    def attr2(self, value: float) -> None:
        self._attr2 = value
        self.push_change_event('attr2', value)
        self.push_archive_event('attr2', value)

    @command(polling_period = 1000)
    def subscribe_to_device(self) -> None:
        # D1 subscribes to D2.attr1
        # D2 subscribes to D1.attr2
        if self.change_subscription_id is None:
            try:
                if self._my_name == self._d1:
                    attr = 'attr1'
                    dev = self._d2
                else:
                    attr = 'attr2'
                    dev = self._d1
                self.info_stream(f'Creating a DeviceProxy for {dev}...')
                self._dp = self.connect_device(dev)
                self.info_stream(f'Connected to {self._dp.name()}.')
                self.info_stream(f'Trying to subscribe to {dev}/{attr}...')
                self.change_subscription_id = self._dp.subscribe_event(attr, EventType.CHANGE_EVENT, self.cb_change_event)
                self.info_stream(f'Subscribed to {dev}/{attr}.')
            except DevFailed as ex:
                # This is expected for as long as the the device is not running yet.
                self.info_stream(f'Sad days, no subscription possible yet. Will try again. Exception: {ex.errors}')

def main(args = None, **kwargs):
    return run((Buttons_device,), **kwargs)

if __name__ == '__main__':
    main()
