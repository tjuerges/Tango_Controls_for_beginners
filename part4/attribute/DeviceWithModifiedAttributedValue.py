'''
Run this Python3 script on your local machine like this:
python3 DeviceWithModifiedAttributedValue.txt.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''

from tango.server import (
    Device,
    attribute,
    command,
    run
)

class DeviceWithModifiedAttributedValue(Device):
    def init_device(self):
        super().init_device()
        self.my_rw_attribute_value = 5.4321

    @attribute
    def my_rw_attribute(self) -> float:
        return self.my_rw_attribute_value

    @my_rw_attribute.write
    def my_rw_attribute(self, value: float = None) -> None:
        self.my_rw_attribute_value = value

    @command
    def modify_value(self):
        # Get the MultiAttribute object. It contains all attributes
        # of this class.
        multi_attr = self.get_device_attr()
        # Now get the attribute that we are interested in.
        attr = multi_attr.get_attr_by_name("my_rw_attribute")
        # And just call set_value on it. This should set the attribute's
        # value.
        # Right?
        # See for yourself and access the device with a client.
        attr.set_value(456)
        attr.set_write_value(123)

def main(args = None, **kwargs):
    return run((DeviceWithModifiedAttributedValue,), **kwargs)

if __name__ == '__main__':
    main()
