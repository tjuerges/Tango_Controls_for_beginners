
'''
Run this Python3 script on your local machine like this:
python3 DeviceWithServerInitHook.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')

From a remote host you can connect to this device via the address that is logged
when the device server for this device starts.
'''
from tango import (
    Util,
    test_context
)

from tango.server import (
    Device,
    DevState,
    run
)

class DeviceWithServerInitHook(Device):
    def init_device(self):
        super().init_device()
        self.set_state(DevState.ON)

    def delete_device(self):
        self.set_state(DevState.OFF)

    def server_init_hook(self):
        try:
            util: Util = Util.instance()
            my_hostname = util.get_host_name()
            my_TRL = self.get_name()
            ds = util.get_dserver_device()
            encoded_ior = util.get_dserver_ior(ds)
            ior = test_context.parse_ior(encoded_ior)
            my_port = ior.port
            self.info_stream(f"Hello! This is your friendly Tango device \"{my_TRL}\" and you can call me under \"tango://{my_hostname}:{my_port}/{my_TRL}#dbase=no\". Have a nice day!")
        except Exception as ex:
            print(f"{ex}")

def main(args = None, **kwargs):
    return run((DeviceWithServerInitHook,), **kwargs)

if __name__ == '__main__':
    main()
