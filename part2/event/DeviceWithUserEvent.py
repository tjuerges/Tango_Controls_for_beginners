'''
Run this Python3 script on your local machine like this:
python3 DeviceWithUserEvent.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''
from tango import (
    DevDouble,
    DevState,
    DevVoid
)
from tango.server import (
    Device,
    attribute,
    command,
    run
)

class DeviceWithUserEvent(Device):
    def init_device(self) -> None:
        super().init_device()
        self._state = 'Yeah!'
        self.set_state(DevState.ON)
        # Both lists are for backwards compatibility
        self.push_event('status', list(), list(), f'Device {self.get_name()} has started.')
        self.set_change_event('status', True, True)
        self.set_archive_event('status', True, True)


    def delete_device(self) -> None:
        self.push_event('status', list(), list(), f'Device {self.get_name()} has stopped.')
        self.set_state(DevState.OFF)
        super().delete_device()

    @command
    def push(self, message: str) -> None:
        self.push_event('status', list(), list(), f'Device {self.get_name()} has this message: {message}')

def main(args = None, **kwargs):
    return run((DeviceWithUserEvent,), **kwargs)

if __name__ == '__main__':
    main()
