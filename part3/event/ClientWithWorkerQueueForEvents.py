"""
First run a PyTango Device that generates CHANGE_EVENTs. For example you could run part2/event/DeviceWithAutomaticPollingLoopAndChangeEvents.py or pogo/DeviceWithRandomDataAttributes.py. Make sure that the Device is either defined in your TangoDB or run it in nodb mode!
Example:
python3 part2/event/DeviceWithAutomaticPollingLoopAndChangeEvents.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Now set an environment variable with the full TRL of the Attribute. (Refer to Tango RFC 19 for an explanation what TRLs are.) If you do not set the environment variable, a default value will be used instead: "tango://127.0.0.1:45678/foo/bar/1/my_rw_attribute#dbase=no"
Then run this script. It will subscribe to the Attribute's CHANGE_EVENT, run for 60s and then automatically exit.
Example:
python3 part3/event/ClientWithWorkerQueueForEvents.py

Finally start an iTango console, connect to the Attribute
ap = tango.AttributeProxy("tango://127.0.0.1:45678/foo/bar/1/my_rw_attrinbute#dbase=no")
and modify the value of the my_rw_attribute
dp.value = 222.22
For this Attribute the abs_change value is 0.1. This means that every modification that is bigger than abs(0.1) will trigger a change event.
The change event will be received by the callback in this script. The callback will then store the event in a queue. The event processor thread will fetch it and log the event's content.

If you would like to check out the error handling in the event processor, then follow
the same steps as above but while this script is running, teminate the device,
for instance by pressing CTRL-C. After 9 seconds the subscriber will generate an error event because it has detected that the device is gone. The error event's reason field is evaluated.
"""

from os import getenv
from threading import Thread
from time import sleep
from logging import (
    basicConfig,
    getLogger,
    INFO,
    WARNING
)
from queue import (
    Empty,
    Queue
)
from tango import (
    AttributeProxy,
    EnsureOmniThread,
    EventType
)

# A device with an interesting Attribute. Allow the caller to pass it as
# an environment variable.
attribute = getenv("ATTRIBUTE", "tango://127.0.0.1:45678/foo/bar/1/my_rw_attribute#dbase=no")


class Callback:
    # A callback class. This allows me to pass a Python Queue where incoming
    # events are stored for processing.
    def __init__(self, _callback_queue: Queue) -> None:
        basicConfig(level = WARNING)
        logger = getLogger("The event callback class")
        # That my queue. Good boy!
        self.callback_queue = _callback_queue
        logger.debug("Callback object created.")

    def callback(self, event) -> None:
        # Never, never, never do anything else in a callback than store the
        # received event somewhere so that a parallel running thread or process
        # can handle the event.
        try:
            # Use a timeout!
            # If storing an element in a Python Queue takes longer than 0.1s,
            # then there is something broken and the user needs to be notified!
            self.callback_queue.put(event, block = True, timeout = 0.1)
        except:
            # This is an exceptional (no pun intended) situation.
            # Therefore I make an exception from the rule that a callback should
            # never log anything. Ever.
            # The problem is that in this case here an event cannot be stored
            # at all, hence there is a serious problem and this needs to be
            # communicated.
            logger.error(f"Unable to store an event for the attribute \"{event.attr_name}\" in the processing queue. This means that this event is lost! This callback will continue as normal. The full event data is as follows: {event}")

def event_processor(callback_queue: Queue) -> None:
    # This thread might perform Tango operations. Hence indicate to Tango that
    # this thread is a Tango thread.
    with EnsureOmniThread():
        # This thread does not inherit an already existing logger. Therefore
        # logging needs to be set up.
        basicConfig(level = INFO)
        logger = getLogger("The event processor")
        # Run for ever in a loop until a no-op event is received.
        # The software that controls this flag decides when this event processor
        # is not needed any more. The processor should not decide this itself.
        logger.debug("The event processor thread is waiting for events...")
        while True:
            # Try to fetch an event from the queue.
            try:
                event = callback_queue.get(block = True)
                if event is not None:
                    logger.info(f"The event processor thread pulled an item from the work queue:\n{event}")
                    # Check if it is an error event.
                    if event.err:
                        error_reasons = []
                        for ex in event.errors:
                            error_reasons.append(ex.reason)
                        logger.warning(f"Received an error event for attribute \"{event.attr_name}\"! The event contains the following Tango exceptions: \"{error_reasons}\"")
                        if "API_EventTimeout" in error_reasons:
                            logger.error(f"The Tango Device is down. It cannot be determined if the Device is temporarily or permanently down. This event subscriber for the attribute \"{event.attr_name}\" will continue with logging this message every 9s until the Tango Device is reponding and the subscription is working again.")
                else:
                    # Time to go. Bye!
                    logger.info(f"The event processor received an empty event. This is the signal to quit. Good bye!")
                    return
            except Empty as e:
                logger.warning(f"The event processor caught an \"{e}\" exception. The exception will be ignored and the event processing continue as normal.")
                pass

# Create a logger.
basicConfig(level = INFO)
logger = getLogger("ClientWithWorkerQueueForEvents")
# Create an event storage for processing of events in a parallel running thread
# or process. Here I am using a Python Queue.
cb_queue = Queue()
# Create a callback object in order to pass the queue to it.
logger.info("Creating the callback object...")
cb = Callback(cb_queue)
logger.info("Creating the worker thread object...")
# Create the event processing thread.
event_processor_thread = Thread(target = event_processor, args=(cb_queue,), name="Event callback processor thread")
logger.info("Starting the worker thread...")
# Start the event processor.
event_processor_thread.start()
# Now subscribe to an event. Any event type will do.
logger.info(f"Subscribing to the change event of attribute \"{attribute}\"...")
# Create am AttributeProxy for the subscription.
ap = AttributeProxy(attribute)
id = ap.subscribe_event(EventType.CHANGE_EVENT, cb.callback)
# Sleep for sleep_time to give the event processor some time to display what it
# does.
sleep_time = 60.0
# Note that for change events to be received, the Attribute needs to change its
# value.
logger.info(f"Sleeping for {sleep_time}s. Please modify the value of \"my_rw_attribute\" now!")
try:
    sleep(sleep_time)
except KeyboardInterrupt as ex:
    pass
logger.info("Unsubscribing from the change event...")
# We are done. Unsubscribe from the event.
ap.unsubscribe_event(id)
logger.info("Stopping and joining the worker thread...")
# Tell the processor to stop.
cb_queue.put(None, block = True, timeout = 0.1)
# Finally wait for the thread to finish.
event_processor_thread.join()
logger.info("Good bye!")
