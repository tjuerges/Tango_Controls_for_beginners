"""
For this to work you need:
- A TangoDB
- The TANGO_HOST environment variable correctly set to point to that TangoDB
- This Device added to the TangoDB.
  Add this Device like so: tango_admin --add-server DeviceWithMemorisedRWAttribute/test DeviceWithMemorisedRWAttribute test/memorised_attribute/1

Then run this Python3 script on your local machine like this:
python3 DeviceWithMemorisedRWAttribute.py test -v4

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy("test/memorised_attribute/1")
"""

import logging
import threading
from time import (
    CLOCK_MONOTONIC,
    clock_gettime,
    sleep
)
from queue import (
    Empty,
    Queue
)
from tango import (
    AttributeProxy,
    EventType
)
from tango.utils import Database


logging.basicConfig(level = logging.INFO)
logger = logging.getLogger('foo')

dev = "test/memorised_attribute/1"
attr = "my_memorised_init_rw_attribute"


class Callback:
    def __init__(self, _callback_queue: Queue) -> None:
        self.callback_queue = _callback_queue
        logger.info("Callback object created.")

    def callback(self, event) -> None:
        logger.info("Callback function called.")
        self.callback_queue.put(event, block = True, timeout = 0.001)
        logger.info("Callback function put event in queue.")

def worker(callback_queue: Queue) -> None:
    logging.basicConfig(level = logging.INFO)
    db = Database()
    logger = logging.getLogger(f"The worker\nTangoDB:\n{db.get_info()}")
    while stop_worker_thread is False:
        try:
            event = None
            event = callback_queue.get(block = True, timeout = 0.5)
            event_ts = clock_gettime(CLOCK_MONOTONIC)
            if event.err is False:
                prop = db.get_device_attribute_property(dev, attr)
                prop_ts = clock_gettime(CLOCK_MONOTONIC)
                event_value = event.attr_value.value
                prop_value = float(prop[attr]["__value"][0])
                logger.info(f"Event:\n{event}\nProperty:\n{prop}\nValues:\nEvent = {event_value}, property = {prop_value}, time difference = {prop_ts - event_ts}s")
        except Empty:
            pass

cb_queue = Queue()
logger.info("Creating the callback object...")
cb = Callback(cb_queue)
logger.info("Creating the worker thread object...")
stop_worker_thread = False
worker_thread = threading.Thread(target = worker, args=(cb_queue,), name="Event callback worker thread")
logger.info("Starting the worker thread...")
worker_thread.start()
logger.info("Subscribing to the change event...")

ap = AttributeProxy(dev + "/" + attr)
id = ap.subscribe_event(EventType.CHANGE_EVENT, cb.callback)

sleep_time = 60.0
logger.info(f"Sleeping for {sleep_time}s. Please modify the value of '{attr}' now!")
try:
    sleep(sleep_time)
except KeyboardInterrupt as ex:
    pass
logger.info("Unsubscribing from the change event...")
ap.unsubscribe_event(id)
logger.info("Stopping and joining the worker thread...")
stop_worker_thread = True
sleep(1.0)
worker_thread.join(1.0)
logger.info("Good bye!")
