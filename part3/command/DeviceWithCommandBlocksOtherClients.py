'''
Run this Python3 script on your local machine like this:
python3 DeviceWithCommandBlocksOtherClients.py test -v4 -nodb -port 45678 -dlist foo/bar/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:45678/foo/bar/1#dbase=no')
'''
from tango import (
    DevDouble,
    DevState,
    DevVoid
)
from tango.server import (
    Device,
    attribute,
    command,
    run
)

import time, datetime


class DeviceWithCommandBlocksOtherClients(Device):
    def init_device(self):
        super().init_device()
        # Set the initial value of my read-only-attribute
        self.__my_ro_attribute_value = 1.2345
        # Set the initial value of my read-write-attribute
        self.__my_rw_attribute_value = 5.4321

        self.set_archive_event('my_ro_attribute', True, True)
        self.set_archive_event('my_rw_attribute', True, True)
        self.set_change_event('my_ro_attribute', True, True)
        self.set_change_event('my_rw_attribute', True, True)

        self.__command_calls = 0
        self.__command_sleep_time = 5

        self.set_state(DevState.ON)

    # This attribute has its minimum difference for a change event set to 0.1.
    @attribute(dtype = DevDouble, archive_abs_change = 0.75, abs_change = 0.1)
    def my_ro_attribute(self) -> DevDouble:
        return self.__my_ro_attribute_value

    @attribute
    def command_sleep_time(self) -> float:
        return self.__command_sleep_time
    @command_sleep_time.write
    def command_sleep_time(self, new_value: float):
        self.__command_sleep_time = new_value

    @attribute
    def number_of_command_executions(self) -> int:
        return self.__command_calls

    @command(dtype_in = DevDouble, dtype_out = DevDouble)
    def modify_ro_attribute_value(self, new_value: DevDouble) -> float:
        self.__command_calls += 1
        self.info_stream(f'Command call no. {self.__command_calls}: sleeping for {self.__command_sleep_time}s...')
        time.sleep(self.__command_sleep_time)
        self.info_stream(f'Command call no. {self.__command_calls}: Sleeping for {self.__command_sleep_time}s done.')
        self.__my_ro_attribute_value = new_value
        self.push_archive_event('my_ro_attribute', new_value)
        self.push_change_event('my_ro_attribute', new_value)
        self.info_stream(f'{datetime.datetime.now()} Command is done.')
        return new_value

    # This attribute has its minimum differencefor a change event set to 0.01.
    @attribute(polling_period = 1000, dtype = DevDouble, archive_abs_change = 0.5, abs_change = 0.01)
    def my_rw_attribute(self) -> DevDouble:
        return self.__my_rw_attribute_value
    @my_rw_attribute.write
    def my_rw_attribute(self, value: DevDouble = None) -> DevVoid:
        self.__my_rw_attribute_value = value
        self.push_archive_event('my_rw_attribute', value)
        self.push_change_event('my_rw_attribute', value)

def main(args = None, **kwargs):
    return run((DeviceWithCommandBlocksOtherClients,), **kwargs)

if __name__ == '__main__':
    main()
