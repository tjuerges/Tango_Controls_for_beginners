'''
Run this Python3 script on your local machine like this:
python3 d2.py test -v4 -nodb -port 50001 -dlist exercise/d2/1

Then connect to the Device from the same machine like this in iTango:
dp = tango.DeviceProxy('tango://127.0.0.1:50001/exercise/d2/1#dbase=no')
'''
from tango import (
    DevDouble,
    DevState,
    DevVoid
)
from tango.server import (
    Device,
    attribute,
    command,
    run
)

from PyTango import (
    InfoIt
)

class D2(Device):
    def init_device(self) -> None:
        super().init_device()
        # Set the initial value of my read-only-attribute
        self._my_ro_attribute_value = 1.2345
        # Set the initial value of my read-write-attribute
        self._my_rw_attribute_value = 5.4321

        self.set_archive_event('d2_my_ro_attribute', True, True)
        self.set_archive_event('d2_my_rw_attribute', True, True)
        self.set_change_event('d2_my_ro_attribute', True, True)
        self.set_change_event('d2_my_rw_attribute', True, True)

        self.set_state(DevState.ON)

    def delete_device(self) -> None:
        self.set_state(DevState.OFF)

    @attribute(archive_abs_change = 0.01, abs_change = 0.01)
    def d2_my_ro_attribute(self) -> float:
        return self._my_ro_attribute_value

    @command
    def d2_modify_ro_attribute_value(self, new_value: float) -> None:
        self._my_ro_attribute_value = new_value
        self.push_archive_event('d2_my_ro_attribute', new_value)
        self.push_change_event('d2_my_ro_attribute', new_value)

    @attribute(archive_abs_change = 0.01, abs_change = 0.01)
    def d2_my_rw_attribute(self) -> float:
        return self._my_rw_attribute_value
    @d2_my_rw_attribute.write
    def d2_my_rw_attribute(self, value: float = None) -> None:
        self._my_rw_attribute_value = value
        self.push_archive_event('d2_my_rw_attribute', value)
        self.push_change_event('d2_my_rw_attribute', value)

    @command
    def d2_modify_rw_attribute_value(self, new_value: float) -> None:
        self._my_rw_attribute_value = new_value
        self.push_archive_event('d2_my_rw_attribute', new_value)
        self.push_change_event('d2_my_rw_attribute', new_value)

    @command
    def modify_d2_my_rw_attribute_abs_change_property(self, new_abs_change: float) -> None:
        multi_attribute = self.get_device_attr()
        the_attribute = multi_attribute.get_attr_by_name('d2_my_rw_attribute')
        attribute_properties = the_attribute.get_properties()
        self.info_stream(f'{new_abs_change} {str(new_abs_change)}')
        attribute_properties.abs_change = str(new_abs_change)
        the_attribute.set_properties(attribute_properties)

def main(args = None, **kwargs):
    return run((D2,), **kwargs)

if __name__ == '__main__':
    main()
