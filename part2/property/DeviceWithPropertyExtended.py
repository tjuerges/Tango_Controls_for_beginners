'''
In order for this example to work, you need to have a TANGO_HOST running!

Set the TANGO_HOST environment variable:
export TANGO_HOST=THE_TANGODB_HOST:10000

Add this DeviceServer to your TangoDB:
tango_admin --add-server DeviceWithPropertyExtended/test DeviceWithPropertyExtended foo/bar/1

Then run this Python3 script:
python3 DeviceWithPropertyExtended.py test

Connect to the Device from iTango:
dp = tango.DeviceProxy('foo/bar/1')

Later you can remove this DeviceServer from the TangoDB:
tango_admin --delete-server DeviceWithPropertyExtended/test --with-properties
'''
from tango import (
    Database,
    DevDouble,
    DevFailed,
    DevState,
    DevString,
    DevVoid
)
from tango.server import (
    Device,
    attribute,
    command,
    class_property,
    device_property,
    run
)

class DeviceWithPropertyExtended(Device):
    # Declare a Class and a Device Property.
    # For this to work, a TANGO_HOST needs to be available and this DeviceServer
    # must have been configured. Read the instructions at the top of this file!
    tango_is_great_property = class_property(
        dtype = DevString,
        default_value = "Tango is great!",
        update_db = True)
    examples_are_great_property = device_property(
        dtype = DevString,
        default_value = "I like examples very much.",
        mandatory = False,
        update_db = True)
    # Uncomment this only if the property exists in your TangoDB.
#     mandatory_device_property = device_property(
#         dtype = DevString,
#         mandatory = True)

    def init_device(self):
        super().init_device()
        # Set the initial value of my read-only-attribute
        self.__my_ro_attribute_value = 1.2345
        # Set the initial value of my read-write-attribute
        self.__my_rw_attribute_value = 5.4321

        self.set_archive_event('my_ro_attribute', True, True)
        self.set_archive_event('my_rw_attribute', True, True)
        self.set_change_event('my_ro_attribute', True, True)
        self.set_change_event('my_rw_attribute', True, True)

        self.set_state(DevState.ON)

    # This attribute has its minimum difference for a change event set to 0.1.
    @attribute(archive_abs_change = 0.75, abs_change = 0.1, min_alarm = -100, max_alarm = 150)
    def my_ro_attribute(self) -> DevDouble:
        return self.__my_ro_attribute_value

    @command(dtype_in = float, dtype_out = DevVoid)
    def modify_ro_attribute_value(self, new_value: DevDouble) -> DevVoid:
        self.__my_ro_attribute_value = new_value
        self.push_archive_event('my_ro_attribute', new_value)
        self.push_change_event('my_ro_attribute', new_value)

    # This attribute has its minimum differencefor a change event set to 0.01.
    @attribute(archive_abs_change = 0.5, abs_change = 0.01, min_alarm = -1.234, max_alarm = 11)
    def my_rw_attribute(self) -> DevDouble:
        return self.__my_rw_attribute_value
    @my_rw_attribute.write
    def my_rw_attribute(self, value: DevDouble = None) -> DevVoid:
        self.__my_rw_attribute_value = value
        self.push_archive_event('my_rw_attribute', value)
        self.push_change_event('my_rw_attribute', value)

def main(args = None, **kwargs):
    return run((DeviceWithPropertyExtended,), **kwargs)

if __name__ == '__main__':
    main()
